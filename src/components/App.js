import React, { useState } from 'react';
import Settings from './Settings';
import StartPage from './StartPage';
// import {
//     BrowserRouter as Router,
//     Switch,
//     Route,
//     Link
//   } from "react-router-dom";


function App() {
      const [isEmptyState, setIsEmptyState] = useState(true);
      const [isAddTripState, setAddTripState] = useState(false);
  
      const triggerAddTripState = () => {
        setIsEmptyState(false);
        setAddTripState(true);
      }

        return (
      
            // <Router>
            //   <div>
            //     <Link to={'settings'}>
            //       <StartPage/>
            //     </Link>

            //     <Switch>
            //         <Route path='settings' Component={Settings}>
            //             <Settings />
            //         </Route>
            //     </Switch>
            //   </div>
            // </Router>
            <div>
                {isEmptyState && <StartPage addTrip={triggerAddTripState} />}
                {isAddTripState && <Settings />}
            </div>
            );
}

export default App;