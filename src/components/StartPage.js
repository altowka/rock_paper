import React from 'react';
import styled from 'styled-components';

const Box = styled.div`
text-align: center;
`;
const Image = styled.img`
transition: .3s ease-in-out;
width: auto;
height: 100vh;
&:hover {
    cursor: pointer;
    opacity: 0.7;
}
`
const StartPage = props => {
     return   (
        <Box>
            <Image onClick={props.addTrip} src="Rock-Paper-Scissors-Cartoon.jpg" alt=""/>
        </Box>
    )

}

export default StartPage;