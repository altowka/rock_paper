import React, { useState } from 'react';
import styled from 'styled-components';

const GameScreen = styled.div`
display: flex;
align-items: center;
justify-content: center;
width: 100%;
margin-bottom: 100px !important;
`;
const GameBox = styled.div`
justify-content: center;
align-items: center;
height: 100vh;
display: flex;
flex-direction: column;
`;

function Game(props) {

    const [honeyPot, setHoneyPot] = useState('');
    const [honeyPot2, setHoneyPot2] = useState('');
    const [winner, setWinner] = useState('');

    function showScores(e) {
        e.preventDefault();

        const options = ["rock", "paper", "scissors"];
        let winnerName = 'buka';
        
        const results = options
            .sort(function() { return .5 - Math.random() }) // Shuffle array
            .slice(0, 2); // Get first 2 items

        //warunki wygranej    
        if (honeyPot === honeyPot2) {
            winnerName = "Draw"
        }

         else if ((honeyPot === "rock" && honeyPot2 === "scissors") || (honeyPot === "scissors" && honeyPot2 === "paper") || (honeyPot === "paper" && honeyPot2 === "rock") ) {
            winnerName = props.player1
         } else {
            winnerName = props.player2
         }
         //zmiana stanu
        setHoneyPot(results[Math.floor(Math.random() * 2)]);    
        setHoneyPot2(results[Math.floor(Math.random() * 2)]);    
        setWinner(winnerName); 

        console.log(honeyPot)
        console.log(honeyPot2)

    }
        return (
            <GameBox>
                    <GameScreen className="ui cards">
                            <div className="card">
                                    <div className="content">
                                    <div className="header">{props.player1}</div>
                                    <div id="score1">{honeyPot}
                                    </div>
                                    </div>
                                </div>
                                <div className="card">
                                    <div className="content">
                                    <div className="header">Winner</div>
                                    <div id="winnerName">{winner}
                                    </div>
                            
                                    </div>
                                </div>
                                <div className="card">
                                    <div className="content">
                                    <div className="header">{props.player2}</div>
                                    <div id="score2">{honeyPot2}
                                    </div>
                                    </div>
                                </div>
        
                    </GameScreen>
                    <button id="button1" className="ui button" type="submit" onClick={showScores}>Play</button>

      </GameBox>     
        )
    
}

export default Game;