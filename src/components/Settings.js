import React from 'react';
import styled from 'styled-components';
import Game from './Game';

const Box = styled.div`
display: flex;
.field {
    margin: 10px !important;
}
`;
const Button = styled.button`
margin-left: 10px!important;
`;
  
class Settings extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            player1: '',
            player2: '',
            isSubmitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
          });
    }

    handleSubmit(event) {
        const button1 = document.getElementById('button1');
         if(this.state.player1 !== '' && this.state.player2 !== '') {
      
            button1.disabled = false;
          

            this.setState({isSubmitted: true})
            const playersForm = document.getElementById('playersForm');
            playersForm.style.display = "none";

        }

        event.preventDefault();
    }
   

    render() {
        return (
            <>
                <div id="playersForm" className="ui segment">
                    <form onSubmit={this.handleSubmit} className="ui form players">
                        <Box>
                            <div className="field">
                                <label>Player 1</label>
                                <input name="player1" type="text" value={this.state.player1} onChange={this.handleChange} />
                            </div>
                            <div className="field">
                                <label>Player 2</label>
                                <input name="player2" type="text" value={this.state.player2} onChange={this.handleChange}/>
                            </div>

                        </Box>
            
                        <Button id="button1" className="ui button" type="submit">Start game</Button>

                    </form>

                </div>
                    {this.state.isSubmitted && <Game player1={this.state.player1} player2={this.state.player2}/>}
y
            </>
        )
    }
    
}


export default Settings;